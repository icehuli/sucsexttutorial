﻿using RGiesecke.DllExport;
using RubyDll;

using VALUE = System.UInt32;
using ID = System.UInt32;

namespace CsExtTutorial
{
    public static class CsExt
    {
        static VALUE mCsExt;
        const string ModuleName = "IHL_CsExt0";

        [DllExport]
        public static VALUE Init_ihl_csext0()
        {

            mCsExt = Ruby.rb_define_module(ModuleName);
            Ruby.rb_define_module_function(mCsExt, "add", add);

            return Ruby.Qnil;
        }

        private static VALUE add(VALUE self, VALUE a, VALUE b)
        {
            int csA = Ruby.rb_num2int(a);
            int csB = Ruby.rb_num2int(b);
            int csC = csA + csB;
            return Ruby.rb_int2inum(csC);
        }

    }
}
