﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;  // for DllImport

using VALUE = System.UInt32; // equivalent to C unsigned long 
using ID = System.UInt32;  // equivalent to C unsigned long 

namespace RubyDll
{
    public static class Ruby
    {

        /// the path of ruby dll
        /// We are using the one loaded by SketchUp.
        /// It is at the same directory as SketchUp excution (SketchUp.exe).
        /// for SketchUp 2013, the default folder is "C:\Program Files (x86)\SketchUp\SketchUp 2013"
        public const string RubyDll = "msvcrt-ruby200";

        ///
        /// Q*
        ///
        public const VALUE Qfalse = (VALUE)0;
        public const VALUE Qtrue  = (VALUE)2;
        public const VALUE Qnil   = (VALUE)4;
        public const VALUE Qundef = (VALUE)6;

        /// 
        /// rb_num2*
        ///
        [DllImport(RubyDll, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 rb_num2int(VALUE val);

        //rb_ *2num
        [DllImport(RubyDll, CallingConvention = CallingConvention.Cdecl)]
        public static extern VALUE rb_int2inum(System.Int32 val);

        ///
        /// module, class
        ///
        [DllImport(RubyDll, CallingConvention = CallingConvention.Cdecl)]
        public static extern VALUE rb_define_module(string name);

        ///
        /// Callback
        /// 
        [UnmanagedFunctionPointerAttribute(CallingConvention.Cdecl)]
        public delegate VALUE CallbackArg2(VALUE self, VALUE arg1, VALUE arg2);


        ///
        /// module method
        ///
        [DllImport(RubyDll, CallingConvention = CallingConvention.Cdecl)]
        private static extern void rb_define_module_function(VALUE module, string name, CallbackArg2 func, int argc);
        public static void rb_define_module_function(VALUE module, string name, CallbackArg2 func)
        {
            MethodDelegates.Add(func);//keep a reference to "func" to prevent it being garbage collected
            rb_define_module_function(module, name, func, 2); 
        }

        //store all defined delegates(callbacks) in a list to keep references to prevent them being garbage collected
        private static List<Delegate> MethodDelegates = new List<Delegate>();
    }
}
