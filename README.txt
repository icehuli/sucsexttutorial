# SketchUp C# Plugins Tutorials

This series of tutorials explains how to write ruby extensions in c# and use them in SketchUp plugins 

## Unmanaged Exports
Robert Giesecke's nuget package "Unmanaged Exports" is used to simplify the work. It helps us to generate the dll entry, 'Init_xxx', for ruby.

https://sites.google.com/site/robertgiesecke/Home/uploads/unmanagedexports; https://www.nuget.org/packages/UnmanagedExports



## Helpful references:

star-engine: http://en.sourceforge.jp/projects/star-engine/scm/svn/tree/99/

ruby-c#: http://magazine.rubyist.net/?0021-RubyWithCSharp

ruby source code: http://rxr.whitequark.org/mri/source

## List
* [CsExt0](Home/CsExt0)
* [CsExt1](Home/CsExt1)
* [CsExtApp0](Home/CsExtApp0)

# License 
The source code is under MIT License 

The MIT License (MIT)

Copyright (c) 2014 ice.huli

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.